#!/bin/bash 

i=9000
n=10000
while [ $i -le $n ]; do
	if [ `factor $i | awk "{print NF}"` == 2 ]; then
		echo $i
	fi
	let i=i+1
done
