#!/bin/bash 

i=2
n=1000
while [ $i -le $n ]; do
	j=2
	is_prime=1
	while [ `expr $j \* $j` -le $i ]; do
		if [ `expr $i % $j` == 0 ]; then
			is_prime=0
			break
		fi
		let j=j+1
	done
	if [ $is_prime == 1 ]; then
		echo $i
	fi
	let i=i+1
done
