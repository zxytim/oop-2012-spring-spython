/*
 * $File: main.cxx
 * $Date: Wed May 30 21:05:53 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "spython.hxx"

int main(int argc, char *argv[])
{
#ifdef __DEBUG_VERBOSE
	fprintf(stderr, "***PROGRAM START***\n");
#endif
	for (int i = 1; i < argc; i ++)
	{
		FILE *file = fopen(argv[i], "r");
		if (file == NULL)
		{
			printf("can not open file %s\n", argv[i]);
			return 1;
		}
		SpythonInterpreter spython;
		spython.interpret(file);

		fclose(file);
	}
	return 0;
}

