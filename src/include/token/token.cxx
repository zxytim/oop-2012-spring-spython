/*
 * $File: token.cxx
 * $Date: Sun May 20 22:27:17 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "token.hxx"
#include <misc.hxx>
#include <syntax_def.hxx>

using std::string;

string TokenSpace::info()
{
	return strprintf("space: type=%d, cnt=%d",
			type, cnt); 
}

string TokenOperator::info()
{
	string oper = "UNKNOWN";
	for (auto &op: OPERATOR)
		if (op.second == opid)
			oper = op.first;
	return strprintf("opid: %d, oper: `%s'", opid, oper.c_str());
}

#if 1
int TokenIdentifier::getTokenID()
{
	for (auto &rw: RESWORD)
		if (name == rw.first)
			return rw.second;
	return TOKEN_IDENTIFIER;
}
#endif

std::string TokenBracket::info()
{
	return strprintf("btype: %s", 
			btype == TOKEN_LBRACKET ? "left" : "right");
}

std::string TokenNewLine::info()
{
	return strprintf("newline: stype=%d, scnt=%d", stype, scnt);
}

