/**
 * $File: token.hxx
 * $Date: Mon May 21 23:36:59 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_TOKEN_DEF__
#define __INCLUDE_TOKEN_DEF__

#include <map>
#include <string>
#include <cstdio>

#include <token_def.hxx>

/**
 * Priority:
 * -1. comment
 * 0. number string newline 
 * 1. space
 * 2. identifier
 * 3. operator
 */

class Token
{
	public:
		int type;
		Token(int type) : type(type) {}
		virtual std::string info() { return "NO INFO"; }
		void printInfo() { fprintf(stderr, "id %d: %s\n", type, info().c_str()); }

		virtual int getTokenID() { return type; }

		virtual ~Token() {}
};


class TokenNumber : public Token
{
	public:
		TokenNumber() : Token(TOKEN_NUMBER) {}
		std::string number;
		int getTokenID() { return TOKEN_OBJ; }
		std::string info() { return "number:" + number; }
};

class TokenString : public Token
{
	public:
		TokenString() : Token(TOKEN_STRING) {}
		std::string str;
		int getTokenID() { return TOKEN_OBJ; }
		std::string info() { return "string: " + str; }
};

class TokenIdentifier : public Token
{
	public:
		TokenIdentifier() : Token(TOKEN_IDENTIFIER) {}
		std::string name;
		std::string info() { return "identifier: " + name; }
		virtual int getTokenID();
};

class TokenNewLine : public Token
{
	public:
		TokenNewLine() : 
			Token(TOKEN_NEWLINE), stype(TOKENSPACE_NONE), scnt(0) {}
		int stype, scnt;
		std::string info();
};



class TokenSpace : public Token
{
	public:
		TokenSpace() : Token(TOKEN_SPACE) {}
		int type, cnt;
		std::string info();
};

const std::map<std::string, int> OPERATOR = {
	{"<", TOKEN_LT},
	{">", TOKEN_GT},
	{"<=", TOKEN_LE},
	{">=", TOKEN_GE},
	{"=", TOKEN_EQ},
	{"!=", TOKEN_NE},
	{"+", TOKEN_PLUS},
	{"-", TOKEN_MINUS},
	{"*", TOKEN_MULT},
	{"/", TOKEN_DIV},
	{"%", TOKEN_MOD},
	{"&", TOKEN_BAND},
	{"|", TOKEN_BOR},
	{"^", TOKEN_BXOR},
	{"~", TOKEN_BREV},
	{"==", TOKEN_LEQ}, 
	{"and", TOKEN_LAND},
	{"or", TOKEN_LOR},
	{"not", TOKEN_LNOT},

	{",", TOKEN_COMMA},
	{":", TOKEN_COLON},
	{";", TOKEN_SEMICOLON},
	{"(", TOKEN_LPARENTH},
	{")", TOKEN_RPARENTH},
	{"**", TOKEN_SSPOW}
	//{"#", TOKEN_SHARP},
};


class TokenOperator : public Token
{
	public:
		TokenOperator() : Token(TOKEN_OPERATOR) {}
		int opid;
		std::string info();
		int getTokenID() { return opid; }
};

class TokenComment : public Token
{
	public:
		TokenComment() : Token(TOKEN_COMMENT) {}
		std::string content;
		std::string info() { return "comment: " + content; }
};

class TokenBracket : public Token
{
	public:
		int btype;
		TokenBracket(int btype) : Token(TOKEN_BRACKET), btype(btype) {}
		TokenBracket() : Token(TOKEN_BRACKET) {}
		int getTokenID() { return btype; }
		std::string info();
};

#endif // __INCLUDE_TOKEN_DEF__
