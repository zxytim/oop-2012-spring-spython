/*
 * $File: vpool.cxx
 * $Date: Wed May 30 20:32:25 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include <exception.hxx>
#include "vpool.hxx"

#include <cstring>

#include "spyobj.hxx"

void VariablePool::assignVarValue(const std::string &name, AutoPtr<SpyObj> value)
{
	auto &data = var_mem[name];

	size_t size = value->dataSize();
	data.size = size;

	data.data = AutoPtr<char>(new char[size], 1);
	memcpy(data.data.ptr(), value.ptr(), size);
}

AutoPtr<SpyObj> VariablePool::fetchVarValue(const std::string &name)
{
	auto it = var_mem.find(name);
	if (it == var_mem.end())
		throw SpyRuntimeExc("no value is assigned for variable %s", name.c_str());

	AutoPtr<SpyObj> obj;
	obj.copyFrom(it->second.data); 
	return obj;
}

VariablePool::~VariablePool()
{
}

