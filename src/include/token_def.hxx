/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     STMT_EMPTY = 258,
     UMINUS = 259,
     TOKEN_NUMBER = 260,
     TOKEN_IDENTIFIER = 261,
     TOKEN_SPACE = 262,
     TOKEN_NEWLINE = 263,
     TOKEN_STRING = 264,
     TOKEN_OPERATOR = 265,
     TOKEN_COMMENT = 266,
     TOKEN_BRACKET = 267,
     TOKEN_LBRACKET = 268,
     TOKEN_RBRACKET = 269,
     TOKEN_VARIABLE = 270,
     TOKEN_FUNCTION = 271,
     TOKEN_LT = 272,
     TOKEN_GT = 273,
     TOKEN_LE = 274,
     TOKEN_GE = 275,
     TOKEN_EQ = 276,
     TOKEN_NE = 277,
     TOKEN_PLUS = 278,
     TOKEN_MINUS = 279,
     TOKEN_MULT = 280,
     TOKEN_DIV = 281,
     TOKEN_MOD = 282,
     TOKEN_BAND = 283,
     TOKEN_BOR = 284,
     TOKEN_BXOR = 285,
     TOKEN_BREV = 286,
     TOKEN_LEQ = 287,
     TOKEN_LAND = 288,
     TOKEN_LOR = 289,
     TOKEN_LNOT = 290,
     TOKEN_COMMA = 291,
     TOKEN_COLON = 292,
     TOKEN_SEMICOLON = 293,
     TOKEN_LPARENTH = 294,
     TOKEN_RPARENTH = 295,
     TOKENRW_PRINT = 296,
     TOKENRW_WHILE = 297,
     TOKENRW_BREAK = 298,
     TOKENRW_IF = 299,
     TOKENRW_ELSE = 300,
     TOKENSPACE_NONE = 301,
     TOKENSPACE_SPACE = 302,
     TOKENSPACE_TAB = 303,
     TOKEN_SSPOW = 304,
     TOKENRW_TRUE = 305,
     TOKENRW_FALSE = 306,
     TOKEN_OBJ = 307,
     IFX = 308
   };
#endif
/* Tokens.  */
#define STMT_EMPTY 258
#define UMINUS 259
#define TOKEN_NUMBER 260
#define TOKEN_IDENTIFIER 261
#define TOKEN_SPACE 262
#define TOKEN_NEWLINE 263
#define TOKEN_STRING 264
#define TOKEN_OPERATOR 265
#define TOKEN_COMMENT 266
#define TOKEN_BRACKET 267
#define TOKEN_LBRACKET 268
#define TOKEN_RBRACKET 269
#define TOKEN_VARIABLE 270
#define TOKEN_FUNCTION 271
#define TOKEN_LT 272
#define TOKEN_GT 273
#define TOKEN_LE 274
#define TOKEN_GE 275
#define TOKEN_EQ 276
#define TOKEN_NE 277
#define TOKEN_PLUS 278
#define TOKEN_MINUS 279
#define TOKEN_MULT 280
#define TOKEN_DIV 281
#define TOKEN_MOD 282
#define TOKEN_BAND 283
#define TOKEN_BOR 284
#define TOKEN_BXOR 285
#define TOKEN_BREV 286
#define TOKEN_LEQ 287
#define TOKEN_LAND 288
#define TOKEN_LOR 289
#define TOKEN_LNOT 290
#define TOKEN_COMMA 291
#define TOKEN_COLON 292
#define TOKEN_SEMICOLON 293
#define TOKEN_LPARENTH 294
#define TOKEN_RPARENTH 295
#define TOKENRW_PRINT 296
#define TOKENRW_WHILE 297
#define TOKENRW_BREAK 298
#define TOKENRW_IF 299
#define TOKENRW_ELSE 300
#define TOKENSPACE_NONE 301
#define TOKENSPACE_SPACE 302
#define TOKENSPACE_TAB 303
#define TOKEN_SSPOW 304
#define TOKENRW_TRUE 305
#define TOKENRW_FALSE 306
#define TOKEN_OBJ 307
#define IFX 308
