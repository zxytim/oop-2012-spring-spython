%{


#include "yacc.hxx"
#include <spython.hxx>

using namespace ast;

%}

%union {
	SpyObj *obj;
	ast::AstNode *node;
}

%nonassoc STMT_EMPTY

//%start TOKEN_NUMBER

%nonassoc UMINUS

%token TOKEN_NUMBER TOKEN_IDENTIFIER TOKEN_SPACE TOKEN_NEWLINE TOKEN_STRING TOKEN_OPERATOR TOKEN_COMMENT TOKEN_BRACKET TOKEN_LBRACKET TOKEN_RBRACKET TOKEN_VARIABLE TOKEN_FUNCTION TOKEN_LT TOKEN_GT TOKEN_LE TOKEN_GE TOKEN_EQ TOKEN_NE TOKEN_PLUS TOKEN_MINUS TOKEN_MULT TOKEN_DIV TOKEN_MOD TOKEN_BAND TOKEN_BOR TOKEN_BXOR TOKEN_BREV TOKEN_LEQ TOKEN_LAND TOKEN_LOR TOKEN_LNOT TOKEN_COMMA TOKEN_COLON TOKEN_SEMICOLON TOKEN_LPARENTH TOKEN_RPARENTH TOKENRW_PRINT TOKENRW_WHILE TOKENRW_BREAK TOKENRW_IF TOKENRW_ELSE TOKENSPACE_NONE TOKENSPACE_SPACE TOKENSPACE_TAB

%token TOKEN_SSPOW

%token TOKENRW_TRUE TOKENRW_FALSE

%token TOKEN_OBJ

%type <obj> TOKEN_OBJ
%type <obj> TOKEN_IDENTIFIER

%type <node> expr
%type <node> expr_nocomp
%type <node> expr_term expr_single

%type <node> stmt stmt_list blocks
%type <node> pstmt pstmt_comma pstmt_end 
%type <node> var

%type <node> if_stmt_all if_stmt_cond 

%type <node> while_stmt_cond while_stmt_all

%nonassoc IFX



/*
expr: have return value
	stmt: do not have return value
*/
%%

program:
	blocks						{ ast_tree->setRoot($1); yydebug("tree constructed"); }
	;

blocks:
	stmt							{ $$ = $1; }
	| blocks stmt					{ $$ = new AstNode({$1, $2}); yydebug("blocks:blocks stmt");}
	;

stmt:
	stmt TOKEN_NEWLINE							{ $$ = $1; yydebug("stmt:stmt <br>"); }
	| expr TOKEN_NEWLINE						{ $$ = $1; yydebug("stmt:expr <br>");}
	| pstmt_end									{ $$ = $1; yydebug("stmt:pstmt_end");}
	| lbracket stmt_list rbracket				{ $$ = $2; yydebug("stmt:{stmt_list}");}
	| lbracket rbracket							{ $$ = new AstNode(); yydebug("stmt:{}");}
	| if_stmt_all								{ $$ = $1; yydebug("stmt:if_stmt_all");}
	| while_stmt_all							{ $$ = $1; yydebug("stmt:while_stmt_all");}
	| TOKENRW_BREAK								{ $$ = new Break();}
	| stmt TOKEN_SEMICOLON						{ $$ = $1; }
	;

pstmt_end:
	pstmt TOKEN_NEWLINE		{ $$ = new Print({$1, new Expr(new SpyString("\n"))}); yydebug("pstmt_end:pstmt <br>");}
	| pstmt_comma TOKEN_NEWLINE		{ $$ = $1; yydebug("pstmt_end:pstmt_comma <br>");}
	| pstmt_token TOKEN_NEWLINE		{ $$ = new Print({new Expr(new SpyString("\n"))}); yydebug("pstmt_end:ps <br>");}
	;

pstmt:
	pstmt_comma expr			{ $$ = new Print({$1, $2}); yydebug("pstmt:pstmt_comma expr"); }
	| pstmt_token expr			{ $$ = new Print({$2}); yydebug("pstmt:pstmt_token expr"); }
	;

pstmt_comma:
	pstmt TOKEN_COMMA			{ $$ = new Print({$1, new Expr(new SpyString(" "))}); yydebug("pstmt_comma:pstmt,");}
	;

pstmt_token:
	TOKENRW_PRINT				{ yydebug("pstmt_token:print"); }
	;

if_stmt_all:
	if_stmt_cond stmt %prec IFX
				{ $$ = new If($1, $2); yydebug("if_stmt_all:if_stmt_cond stmt");}
	| if_stmt_cond stmt else_prompt stmt
				{ $$ = new If($1, $2, $4); yydebug("if_stmt_all:if_stmt_cond stmt else: stmt"); }
	;

else_prompt:
	TOKENRW_ELSE TOKEN_COLON
	| else_prompt TOKEN_NEWLINE
	;

if_stmt_cond:
	TOKENRW_IF expr TOKEN_COLON		{ $$ = $2; yydebug("if_stmt_cond:if expr:");}
	| if_stmt_cond TOKEN_NEWLINE	{ $$ = $1; yydebug("if_stmt_cond:if_stmt_cond <br>");}
	;

while_stmt_all:
	while_stmt_cond stmt { $$ = new While($1, $2); yydebug("while_stmt_all:while_stmt_cond stmt"); }

while_stmt_cond:
	TOKENRW_WHILE expr TOKEN_COLON	{ $$ = $2; yydebug("while_stmt_cond:while expr:");}
	| while_stmt_cond TOKEN_NEWLINE	{ $$ = $1; yydebug("while_stmt_cond:while_stmt_cond <br>");}
	;

expr:
	expr_nocomp								{ $$ = $1; }
	| expr_nocomp TOKEN_LT expr_nocomp		{ $$ = new OperLT($1, $3); }
	| expr_nocomp TOKEN_GT expr_nocomp		{ $$ = new OperGT($1, $3); }
	| expr_nocomp TOKEN_GE expr_nocomp		{ $$ = new OperGE($1, $3); }
	| expr_nocomp TOKEN_LE expr_nocomp		{ $$ = new OperLE($1, $3); }
	| expr_nocomp TOKEN_LEQ expr_nocomp		{ $$ = new OperLEQ($1, $3); }
//	| expr_nocomp TOKEN_LAND expr_nocomp	{ $$ = new CompareAND($1, $3); }
//	| expr_nocomp TOKEN_LOR expr_nocomp		{ $$ = new CompareOR($1, $3); }
//	| expr_nocomp TOKEN_LNOT expr_nocomp	{ $$ = new CompareNOT($1, $3); }
	;

expr_nocomp:
	var TOKEN_EQ expr_nocomp			{ $$ = new Assign($1, $3); yydebug("expr:var = expr");} 
	| expr_nocomp TOKEN_PLUS expr_term { $$ = new OperPlus($1, $3); yydebug("expr:expr + term");}
	| expr_nocomp TOKEN_MINUS expr_term { $$ = new OperMinus($1, $3); yydebug("expr:expr - term");}
	| expr_term					{ $$ = $1; yydebug("expr:expr_term");}
	;

expr_term:
	expr_term TOKEN_MULT expr_single { $$ = new OperMult($1, $3); yydebug("expr_term:term*single");}
	| expr_term TOKEN_DIV expr_single { $$ = new OperDiv($1, $3); yydebug("expr_term:term/single");}
	| expr_term TOKEN_MOD expr_single { $$ = new OperMod($1, $3); yydebug("expr_term:term/single");}
	| expr_single					{ $$ = $1; yydebug("expr_term:expr_single");}
	| TOKEN_MINUS expr_term	%prec UMINUS { $$ = new OperUnaryMinus($2); }
	;

expr_single:
	var							{ $$ = new Expr({$1}); yydebug("expr_single:var"); }
	| TOKEN_OBJ					{ $$ = new Expr($1); yydebug("expr_single:obj");}
	| TOKENRW_TRUE				{ $$ = new Expr(new SpyInteger(1)); yydebug("expr_single:true");}
	| TOKENRW_FALSE				{ $$ = new Expr(new SpyInteger(0)); yydebug("expr_single:false");}
	| TOKEN_LPARENTH expr TOKEN_RPARENTH { $$ = $2; yydebug("expr_single:(expr)");}
	| expr_single TOKEN_SSPOW expr_single { $$ = new OperPower($1, $3); yydebug("expr_term:term**single"); }
	;

var:
	TOKEN_IDENTIFIER			{ $$ = new Variable($1); yydebug("var:id");}


stmt_list:
	stmt						{ $$ = $1; yydebug("stmt_list:stmt");}
	| stmt_list stmt			{ $$ = new AstNode({$1, $2}); yydebug("stmt_list:stmt_list stmt"); }
	;

lbracket:
	TOKEN_LBRACKET					{ yydebug("lbracket:{");}
	| lbracket TOKEN_NEWLINE		{ yydebug("lbracket:{_");}
	;

rbracket:
	TOKEN_RBRACKET					{ yydebug("rbracket:}");}
	;

/**
 * vim: ft=cpp
 */
