/*
 * $File: yacc.hxx
 * $Date: Wed May 23 22:44:27 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_YACC_SPYTHON__
#define __INCLUDE_YACC_SPYTHON__

#include <vector>
#include <spyobj.hxx>
#include <token.hxx>
#include <auto_ptr.hxx>
#include <ast.hxx>

//#define YYTOKENTYPE

class yystype 
{
	public:
		AutoPtr<SpyObj>obj;
		AutoPtr<ast::AstNode> node;
};

#define YYSTYPE yystype

class SpythonInterpreter;

namespace YACCImpl
{
	extern SpythonInterpreter *intprtr;
	extern size_t cur_tokid;
	extern AutoPtr<Token> cur_token;
	void yyinit(SpythonInterpreter *intprtr_);
	int yylex();
	void yyerror(const char *msg);
	void pause();

	extern AutoPtr<ast::Ast> ast_tree;
}

extern YYSTYPE yylval;

int yaccparse(AutoPtr<ast::Ast> ast);
int yyparse();
void yydebug(const char *fmt, ...);

using namespace YACCImpl;

#endif // __INCLUDE_YACC_SPYTHON__
