/*
 * $File: yacc.cxx
 * $Date: Wed May 23 22:45:50 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "yacc.hxx"
#include <exception.hxx>
#include <../spython.hxx>

size_t YACCImpl::cur_tokid;
AutoPtr<Token> YACCImpl::cur_token;
SpythonInterpreter *YACCImpl::intprtr;
AutoPtr<ast::Ast> YACCImpl::ast_tree;

int yaccparse(AutoPtr<ast::Ast> astt)
{
	ast_tree = astt;
	return yyparse();
}


void YACCImpl::yyinit(SpythonInterpreter *intprtr_)
{
	intprtr = intprtr_;
	cur_tokid = 0;
}

int YACCImpl::yylex()
{
	if (cur_tokid < intprtr->tokens.size())
	{
		cur_token = intprtr->tokens[cur_tokid];


		int id = cur_token->getTokenID();

		// assign yylval.obj
		yylval.obj = NULL;
		if (id == TOKEN_OBJ)
		{
			std::string tmp;
			if (cur_token->type == TOKEN_NUMBER)
			{
				TokenNumber *tnum = dynamic_cast<TokenNumber *>(cur_token.ptr());
				yylval.obj = new SpyInteger(tnum->number);
			}
			else if (cur_token->type == TOKEN_STRING)
			{
				TokenString *tstr = dynamic_cast<TokenString *>(cur_token.ptr());
				yylval.obj = new SpyString(tstr->str);
			}
		}
		else if (id == TOKEN_IDENTIFIER)
		{
			TokenIdentifier *tid = dynamic_cast<TokenIdentifier *>(cur_token.ptr());
			yylval.obj = new SpyVariable(tid->name);
		}

#ifdef __DEBUG_VERBOSE
		cur_token->printInfo();
#endif

		return intprtr->tokens[cur_tokid ++]->getTokenID();
	}
	return 0;
}

void YACCImpl::yyerror(const char *msg)
{
	//printf("XXX: %s\n", msg);
	throw SpyExc((std::string)("YYError: ") + msg);
}

void YACCImpl::pause()
{
	printf("paused\n");
}

void yydebug(const char *fmt, ...)
{
#ifndef __DEBUG_VERBOSE
	return;
#endif 
	static const int MSG_LEN_MAX = 100000;
	static char msg_buf[MSG_LEN_MAX + 1];
	va_list args;
	va_start(args, fmt);
	vsnprintf(msg_buf, MSG_LEN_MAX, fmt, args);
	va_end(args);

	fprintf(stderr, "[YYDEBUG] %s\n", msg_buf);
}


