/*
 * $File: spyobj.hxx
 * $Date: Sat May 26 01:45:16 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_SPYOBJ__
#define __INCLUDE_SPYOBJ__

#include <set>
#include <cstdio>
#include <string>
#include <vector>

#include "id.hxx"

#include <vpool.hxx>

class SpyString;

class SpyFunction;
/**
 * SpyObj is the type that can be assigned value
 */
class SpyObj
{
	public:

		virtual std::string name();
		std::vector<AutoPtr<SpyFunction> > methods;

		virtual size_t dataSize();

		virtual SpyString toString() const;

		AutoPtr<SpyObj> callMethod(
				const std::string &func_name, 
				AutoPtr<VariablePool> vpool, 
				std::vector<AutoPtr<SpyObj>> params);

		/**
		 * SpyObj is responsible for 
		 * memory allocation for 
		 * pointer in VariablePool::Data
		 *
		 */
		virtual VariablePool::Data serialize();
		virtual void unserialize(const VariablePool::Data &data);

		virtual ~SpyObj() {}
};

#include <vector>

class SpyFunctionParamList : public SpyObj
{
	public:
		std::vector<AutoPtr<SpyObj>> params;
};

class SpyFunction : public SpyObj
{
	public:
		std::string name;
		SpyFunction(const std::string &name);

		AutoPtr<SpyObj> execute(
				const std::string &func_name, 
				AutoPtr<VariablePool> vpool, 
				std::vector<AutoPtr<SpyObj>> params);

};


#endif // __INCLUDE_SPYOBJ__
