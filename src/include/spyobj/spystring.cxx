/*
 * $File: spystring.cxx
 * $Date: Thu May 24 12:03:52 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "spystring.hxx"

SpyString::SpyString()
{
}

SpyString::SpyString(const char *str) :
	std::string(str)
{
}

SpyString::SpyString(const std::string &str) :
	std::string(str)
{
}

SpyString SpyString::toString() const
{
	return *this;
}

size_t SpyString::dataSize()
{
	return sizeof(*this);
}

