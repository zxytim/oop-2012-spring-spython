/*
 * $File: spyinteger.hxx
 * $Date: Wed May 23 11:29:57 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_SPYINTEGER__
#define __INCLUDE_SPYINTEGER__

#include "spyobj.hxx"
#include "spystring.hxx"

class SpyInteger : public SpyObj
{
	protected:
		long long int num;

		void init();
	public:
		SpyInteger();
		SpyInteger(long long int integer);

		SpyInteger(const SpyInteger &integer);

		SpyInteger(const std::string &str);

		SpyInteger operator + (const SpyInteger &integer) const;
		SpyInteger operator - (const SpyInteger &integer) const;
		SpyInteger operator * (const SpyInteger &integer) const;
		SpyInteger operator / (const SpyInteger &integer) const;
		SpyInteger operator % (const SpyInteger &integer) const;

		bool operator == (const SpyInteger &integer) const;
		bool operator != (const SpyInteger &integer) const;

		bool operator < (const SpyInteger &integer) const;
		bool operator > (const SpyInteger &integer) const;
		bool operator <= (const SpyInteger &integer) const;
		bool operator >= (const SpyInteger &integer) const;

		SpyInteger& operator += (const SpyInteger &integer);
		SpyInteger& operator -= (const SpyInteger &integer);
		SpyInteger& operator *= (const SpyInteger &integer);
		SpyInteger& operator /= (const SpyInteger &integer);
		SpyInteger operator %= (const SpyInteger &integer) const;

		SpyString toString() const;

		virtual size_t dataSize();
};

//typedef long long int SpyInteger;

#endif // __INCLUDE_SPYINTEGER__
