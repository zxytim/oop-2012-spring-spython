/*
 * $File: spystring.hxx
 * $Date: Sun May 20 20:47:19 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_SPYSTRING__
#define __INCLUDE_SPYSTRING__

#include "spyobj.hxx"
#include <string>

class SpyString : public std::string, public SpyObj
{
	public:
		SpyString();
		SpyString(const std::string &str);
		SpyString(const char *str);

		virtual SpyString toString() const;
		virtual size_t dataSize();

};

#endif // __INCLUDE_SPYSTRING__
