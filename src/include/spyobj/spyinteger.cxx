/*
 * $File: spyinteger.cxx
 * $Date: Sat May 26 01:47:12 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "spyinteger.hxx"
#include "spystring.hxx"

#include <exception.hxx>

SpyInteger::SpyInteger()
{
	num = 0;
}

SpyInteger::SpyInteger(long long int integer)
{
	num = integer;
	init();
}

SpyInteger::SpyInteger(const SpyInteger &integer)
{
	*this = integer;
	init();
}

SpyInteger::SpyInteger(const std::string &str)
{
	num = 0;
	sscanf(str.c_str(), "%lld", &num);
	init();
}

namespace SpyIntegerImpl
{
#if 0
	class TwoMethod: public SpyObj::Method
	{
		public:
			TwoMethod(const std::string &name) : SpyObj::Method(name) {}
			AutoPtr<SpyObj>call(AutoPtr<SpyObj>, const std::vector<AutoPtr<SpyObj>> &params)
			{
				if (params.size() != 1)
					throw SpyRuntimeExc("PlusOperator");
				return NULL;
			};
	};
#endif
};
void SpyInteger::init()
{
}


#define DEF_ARITH_OPER(OPER) \
	SpyInteger SpyInteger::operator OPER (const SpyInteger &integer) const \
	{ \
		return SpyInteger(num OPER integer.num); \
	} \

DEF_ARITH_OPER(+)
DEF_ARITH_OPER(-)
DEF_ARITH_OPER(*)

#undef DEF_ARITH_OPER
SpyInteger SpyInteger::operator / (const SpyInteger &integer) const
{
	if (integer.num == 0)
		throw SpyRuntimeExc("integer divided by zero");
	return SpyInteger(num / integer.num);
}


SpyInteger SpyInteger::operator % (const SpyInteger &integer) const
{
	if (integer.num == 0)
		throw SpyRuntimeExc("integer modulo by zero");
	return SpyInteger(num % integer.num);
}

bool SpyInteger::operator == (const SpyInteger &integer) const
{
	return num == integer.num;
}

bool SpyInteger::operator != (const SpyInteger &integer) const
{
	return num != integer.num;
}

#define DEF_OPER(OPER) \
	bool SpyInteger::operator OPER (const SpyInteger &integer) const \
	{ \
		return num OPER integer.num; \
	} \

DEF_OPER(<);
DEF_OPER(>);
DEF_OPER(<=);
DEF_OPER(>=);

#undef DEF_OPER

#include <sstream>

using namespace std;

SpyString SpyInteger::toString() const
{
	string str;
	stringstream ss;
	ss << num;
	ss >> str;
	return str;
}

size_t SpyInteger::dataSize()
{
	return sizeof(*this);
}

