/*
 * $File: id.hxx
 * $Date: Sun May 20 20:15:45 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_SPYID__
#define __INCLUDE_SPYID__

enum SpyObjID
{
	SPYPRINTABLE,
	SPYSTRING,
	SPYINTEGER,
	SPYVARIABLE,
	SPYVOID,
};

#endif // __INCLUDE_SPYID__
