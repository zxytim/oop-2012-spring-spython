/*
 * $File: spyfunction.cxx
 * $Date: Sat May 26 01:46:40 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "spyobj.hxx"

AutoPtr<SpyObj> SpyFunction::execute(
		const std::string &func_name, 
		AutoPtr<VariablePool> vpool, 
		std::vector<AutoPtr<SpyObj>> params)
{
}

SpyFunction::SpyFunction(const std::string &name) :
	name(name)
{
}
