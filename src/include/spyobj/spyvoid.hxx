/*
 * $File: spyvoid.hxx
 * $Date: Sun May 20 20:23:53 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_SPYVOID__
#define __INCLUDE_SPYVOID__

#include "spyobj.hxx"

class SpyVoid : public SpyObj
{
	public:
		SpyVoid();
};

#endif // __INCLUDE_SPYVOID__
