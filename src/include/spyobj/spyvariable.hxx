/*
 * $File: spyvariable.hxx
 * $Date: Sun May 20 15:21:11 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_SPYVARIABLE__
#define __INCLUDE_SPYVARIABLE__

#include <string>
#include "spyobj.hxx"

class SpyVariable : public SpyObj
{
	public:
		std::string name;
		SpyVariable(const std::string &name);
};

#endif // __INCLUDE_SPYVARIABLE__
