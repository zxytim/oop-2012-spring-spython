/*
 * $File: spyobj.cxx
 * $Date: Sat May 26 01:46:25 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include <exception.hxx>

#include "spyobj.hxx"
#include "spystring.hxx"
#include "spyfunction.hxx"

size_t SpyObj::dataSize()
{
	throw SpyRuntimeExc("can not determine object size");
}

SpyString SpyObj::toString() const
{
	return "toString() unimplemented.";
}

AutoPtr<SpyObj> SpyObj::callMethod(
		const std::string &func_name, 
		AutoPtr<VariablePool> vpool,
		std::vector<AutoPtr<SpyObj>> params)
{
	for (auto &m: methods)
		if (m->name == func_name)
		{
			// XXX: put self into params
			return m->execute("__call__", vpool, params);
		}
	throw SpyRuntimeExc("can not call method function `%s'", func_name.c_str());
}

std::string SpyObj::name()
{
	return "SpyObj";
}

VariablePool::Data SpyObj::serialize()
{
}

void SpyObj::unserialize(const VariablePool::Data &data)
{
}

