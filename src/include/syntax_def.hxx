/*
 * $File: syntax_def.hxx
 * $Date: Mon May 21 19:25:43 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_SYNTAX_DEF__
#define __INCLUDE_SYNTAX_DEF__

#include <token_def.hxx>


const std::map<std::string, int> RESWORD = {
	{"print", TOKENRW_PRINT},
	{"while", TOKENRW_WHILE},
	{"break", TOKENRW_BREAK},
	{"if", TOKENRW_IF},
	{"else", TOKENRW_ELSE},
	{"True", TOKENRW_TRUE},
	{"False", TOKENRW_FALSE}
};

#endif // __INCLUDE_SYNTAX_DEF__
