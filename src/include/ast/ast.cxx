/*
 * $File: ast.cxx
 * $Date: Wed May 30 20:34:49 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */


#include "ast.hxx"
#include <exception.hxx>
#include <cstring>

using namespace ast;

using namespace std;


AstNode::AstNode()
{
	value = NULL;
}

AstNode::AstNode(const std::vector<AutoPtr<AstNode>> &child) :
	child(child) 
{
	value = NULL;
}

AutoPtr<SpyObj> AstNode::execute(AutoPtr<VariablePool> vpool)
{
#ifdef __DEBUG_VERBOSE
	fprintf(stderr, "walk:AstNode\n");
#endif 

	AutoPtr<SpyObj> ret = new SpyVoid();
	for (auto &chd: child)
		ret = chd->execute(vpool);

	return ret;
	//throw SpyRuntimeExc("function excute not implemented"); 
}

void Ast::setRoot(AutoPtr<AstNode>root)
{
	this->root = root;
}

void Ast::execute()
{
	AutoPtr<VariablePool> vpool = new VariablePool;
	root->execute(vpool);
}

Ast::~Ast()
{
}
