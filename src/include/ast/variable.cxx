/*
 * $File: variable.cxx
 * $Date: Wed May 23 21:57:58 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "variable.hxx"

#include <sassert.hxx>

using namespace ast;

Variable::Variable(AutoPtr<SpyObj>obj)
{
	AutoPtr<SpyVariable> var;
	var.copyFrom(obj);

	name = var->name;
}


AutoPtr<SpyObj>Variable::execute(AutoPtr<VariablePool> vpool)
{
	assert(child.size() == 0);
	return vpool->fetchVarValue(name);
}


