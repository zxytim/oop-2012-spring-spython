/*
 * $File: assign.hxx
 * $Date: Wed May 23 21:54:51 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_ASSIGN__
#define __INCLUDE_AST_ASSIGN__

#include "expr.hxx"

#include "variable.hxx"

namespace ast
{
	class Assign : public Expr
	{
		public:
			AutoPtr<Variable> var;
			Assign(AutoPtr<AstNode>var, AutoPtr<AstNode>val);
			virtual AutoPtr<SpyObj>execute(AutoPtr<VariablePool> vpool);
	};
}


#endif // __INCLUDE_AST_ASSIGN__
