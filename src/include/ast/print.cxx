/*
 * $File: print.cxx
 * $Date: Thu May 24 10:43:11 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "print.hxx"

#include <sassert.hxx>
#include <exception.hxx>

using namespace ast;

AutoPtr<SpyObj> Print::execute(AutoPtr<VariablePool> vpool)
{
#ifdef __DEBUG_VERBOSE
	fprintf(stderr, "walk:Print\n");
#endif

	if (child.size())
	{
		for (size_t i = 0; i < child.size() - 1; i ++)
			child[i]->execute(vpool);
		std::string content;
		content = child.back()->execute(vpool)->toString();
		printf("%s", content.c_str());
	}
	return new SpyVoid();
}

