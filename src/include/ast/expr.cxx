/*
 * $File: expr.cxx
 * $Date: Thu May 24 11:50:25 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "expr.hxx"

#include <sassert.hxx>

using namespace ast;

Expr::Expr()
{
}

Expr::Expr(const std::vector<AutoPtr<AstNode>> &child) :
	AstNode(child)
{
}

AutoPtr<SpyObj>Expr::execute(AutoPtr<VariablePool> vpool)
{
#ifdef __DEBUG_VERBOSE
	fprintf(stderr, "walk:expr\n");
#endif 

	if (child.size() == 0) // unit element
		return value;

	AutoPtr<SpyObj>ret;
	for (auto &chd: child)
		ret = chd->execute(vpool);
	return ret;
}

