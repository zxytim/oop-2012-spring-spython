/*
 * $File: while.cxx
 * $Date: Wed May 23 21:58:50 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "while.hxx"

using namespace ast;

Break::Break()
{
}

AutoPtr<SpyObj>Break::execute(AutoPtr<VariablePool> vpool)
{
	throw CtrlFlowBreak();
}

While::While(AutoPtr<AstNode>expr_, AutoPtr<AstNode> stmt_)
{
	expr.copyFrom(expr_);
	if (expr == NULL)
		throw SpyRuntimeExc("`while' condition must be an expression");
	stmt = stmt_;
}

AutoPtr<SpyObj>While::execute(AutoPtr<VariablePool> vpool)
{
	try
	{
		for (;;)
		{
			AutoPtr<SpyInteger> expr_val;
			expr_val.copyFrom(expr->execute(vpool));
			if (expr_val == NULL)
				throw SpyRuntimeExc("while condition must be integer type");
			if ((*expr_val) == 0)
				break;
			stmt->execute(vpool);
		}
	}
	catch (CtrlFlowBreak )
	{
#ifdef __DEBUG_VERBOSE
		fprintf(stderr, "break in `while'.\n");
#endif
	}
	return new SpyVoid();
}

