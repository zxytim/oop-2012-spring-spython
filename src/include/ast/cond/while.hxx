/*
 * $File: while.hxx
 * $Date: Mon May 21 19:11:28 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_WHILE__
#define __INCLUDE_AST_WHILE__

#include "cond.hxx"

namespace ast
{
	class CtrlFlowBreak : public CtrlFlow
	{
	};

	class Break : public AstNode
	{
		public:
			Break();
			virtual AutoPtr<SpyObj>execute(AutoPtr<VariablePool> vpool);
	};

	class While : public AstNode
	{
		public:
			AutoPtr<Expr>expr;
			AutoPtr<AstNode>stmt;
			While(AutoPtr<AstNode>expr, AutoPtr<AstNode> stmt);
			virtual AutoPtr<SpyObj>execute(AutoPtr<VariablePool> vpool);
	};
}

#endif // __INCLUDE_AST_WHILE__
