/*
 * $File: if.cxx
 * $Date: Wed May 30 20:45:49 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "if.hxx"

using namespace ast;

If::If(AutoPtr<AstNode>expr_, AutoPtr<AstNode>stmt_true_, AutoPtr<AstNode>stmt_false_)
{
	expr.copyFrom(expr_);
	if (expr == NULL)
		throw SpyRuntimeExc("`if' condition must be an expression");
	stmt_true.copyFrom(stmt_true_);
	stmt_false.copyFrom(stmt_false_);
}

AutoPtr<SpyObj>If::execute(AutoPtr<VariablePool> vpool)
{
	AutoPtr<SpyInteger> expr_val;
	expr_val.copyFrom(expr->execute(vpool));

	if (!expr_val.isTypeOf(SpyInteger()))
		throw SpyRuntimeExc("if condition must be integer type");
	if ((*expr_val) != 0)
		stmt_true->execute(vpool);
	else if (stmt_false != NULL)
		stmt_false->execute(vpool);
	return new SpyVoid();
}

