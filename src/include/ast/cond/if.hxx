/*
 * $File: if.hxx
 * $Date: Wed May 23 22:00:07 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_IF__
#define __INCLUDE_AST_IF__

#include "cond.hxx"

namespace ast
{
	class If : public AstNode
	{
		public:
			AutoPtr<Expr>expr;
			AutoPtr<AstNode>stmt_true, stmt_false;
			If(AutoPtr<AstNode>expr, AutoPtr<AstNode> stmt_true, AutoPtr<AstNode> stmt_false = NULL);
			virtual AutoPtr<SpyObj>execute(AutoPtr<VariablePool> vpool);
	};
}

#endif // __INCLUDE_AST_IF__
