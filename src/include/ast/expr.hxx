/*
 * $File: expr.hxx
 * $Date: Wed May 23 22:05:04 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_EXPR__
#define __INCLUDE_AST_EXPR__

#include "ast.hxx"

namespace ast
{
	class Expr : public AstNode
	{
		public:
			Expr();
			Expr(const std::vector<AutoPtr<AstNode>> &child);
			Expr(AutoPtr<SpyObj>value) { this->value = value; }
			virtual AutoPtr<SpyObj>execute(AutoPtr<VariablePool> vpool);
	};
}


#endif // __INCLUDE_AST_EXPR__
