/*
 * $File: binary_oper.cxx
 * $Date: Wed May 23 21:53:56 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "binary_oper.hxx"

#include <sassert.hxx>
#include <exception.hxx>

using namespace ast;

BinaryOper::BinaryOper(AutoPtr<AstNode>oprnd0, AutoPtr<AstNode>oprnd1)
{
	op0.copyFrom(oprnd0);
	op1.copyFrom(oprnd1);

	if (op0 == NULL || op1 == NULL)
		throw SpyRuntimeExc("operands of binary operator must be expression");
}

AutoPtr<SpyObj>BinaryOper::execute(AutoPtr<VariablePool> vpool)
{
	AutoPtr<SpyObj> obj0, obj1;
	obj0.copyFrom(op0->execute(vpool));
	obj1.copyFrom(op1->execute(vpool));
	return calc(obj0, obj1);
}
