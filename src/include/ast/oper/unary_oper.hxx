/*
 * $File: unary_oper.hxx
 * $Date: Mon May 21 21:47:04 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_BINARY_UNARY_OPER__
#define __INCLUDE_AST_BINARY_UNARY_OPER__

#include "oper.hxx"

#include <spyobj.hxx>
#include <exception.hxx>

namespace ast
{
	class OperUnaryMinus: public Expr
	{
		public:
			AutoPtr<Expr>oprnd;
			OperUnaryMinus(AutoPtr<AstNode>oprnd);
			virtual AutoPtr<SpyObj>execute(AutoPtr<VariablePool> vpool);
	};
}

#endif // __INCLUDE_AST_BINARY_UNARY_OPER__
