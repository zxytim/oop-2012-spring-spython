/*
 * $File: binary_oper.hxx
 * $Date: Thu May 24 13:20:19 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_BINARY_OPER__
#define __INCLUDE_AST_BINARY_OPER__

#include "oper.hxx"
#include <spyobj.hxx>

#include <exception.hxx>

namespace ast
{
	class BinaryOper : public Expr
	{
		protected:
			virtual AutoPtr<SpyObj>calc(
					AutoPtr<SpyObj>op0,
					AutoPtr<SpyObj>op1) = 0;
		public:
			AutoPtr<Expr>op0, op1;
			BinaryOper(AutoPtr<AstNode>oprnd0, AutoPtr<AstNode>oprnd1);
			virtual AutoPtr<SpyObj>execute(AutoPtr<VariablePool> vpool);
	};

#define DEF_OPER(CNAME, OPER, TYPE, ID, RETTYPE) \
	class CNAME : public BinaryOper \
{ \
	protected: \
			   AutoPtr<SpyObj>calc( \
					   AutoPtr<SpyObj>op0, \
					   AutoPtr<SpyObj>op1) \
	{ \
		if (op0.isTypeOf(TYPE()) && op1.isTypeOf(TYPE())) \
		{ \
			AutoPtr<TYPE> top0, top1; \
			top0.copyFrom(op0), \
			top1.copyFrom(op1); \
			return new RETTYPE((*top0) OPER (*top1)); \
		} else \
		{ \
			throw SpyRuntimeExc("operator `" #OPER "' can not be applied"); \
		} \
		return new SpyVoid(); \
	} \
	public: \
			CNAME (AutoPtr<AstNode>oprnd0, AutoPtr<AstNode>oprnd1) : \
	BinaryOper(oprnd0, oprnd1) {} \
};

#define DEF_OPER2(CNAME, OPER, TYPE0, ID0, RETTYPE0, TYPE1, ID1, RETTYPE1) \
	class CNAME : public BinaryOper \
{ \
	protected: \
			   AutoPtr<SpyObj>calc( \
					   AutoPtr<SpyObj>op0, \
					   AutoPtr<SpyObj>op1) \
	{ \
		if (op0.isTypeOf(TYPE0()) && op1.isTypeOf(TYPE0())) \
		{ \
			AutoPtr<TYPE0> top0, top1; \
			top0.copyFrom(op0); \
			top1.copyFrom(op1); \
			return new RETTYPE0((*top0) OPER (*top1)); \
		} \
		else if (op0.isTypeOf(TYPE1()) && op1.isTypeOf(TYPE1())) \
		{ \
			AutoPtr<TYPE1> top0, top1; \
			top0.copyFrom(op0); \
			top1.copyFrom(op1); \
			return new RETTYPE1((*top0) OPER (*top1)); \
		} else \
		{ \
			throw SpyRuntimeExc("operator `" #OPER "' can not be applied"); \
		} \
		return new SpyVoid(); \
	} \
	public: \
			CNAME (AutoPtr<AstNode>oprnd0, AutoPtr<AstNode>oprnd1) : \
	BinaryOper(oprnd0, oprnd1) {} \
};

// arithmetic
DEF_OPER(OperDiv, /, SpyInteger, SPYINTEGER, SpyInteger);
DEF_OPER(OperMult, *, SpyInteger, SPYINTEGER, SpyInteger);
DEF_OPER(OperMod, %, SpyInteger, SPYINTEGER, SpyInteger);
DEF_OPER2(OperPlus, +, SpyInteger, SPYINTEGER, SpyInteger, SpyString, SPYSTRING, SpyString);
DEF_OPER(OperMinus, -, SpyInteger, SPYINTEGER, SpyInteger);

// compare
DEF_OPER2(OperLT, <, SpyInteger, SPYINTEGER, SpyInteger, SpyString, SPYSTRING, SpyInteger);
DEF_OPER2(OperGT, >, SpyInteger, SPYINTEGER, SpyInteger, SpyString, SPYSTRING, SpyInteger);
DEF_OPER2(OperLE, <=, SpyInteger, SPYINTEGER, SpyInteger, SpyString, SPYSTRING, SpyInteger);
DEF_OPER2(OperGE, >=, SpyInteger, SPYINTEGER, SpyInteger, SpyString, SPYSTRING, SpyInteger);
DEF_OPER2(OperLEQ, ==, SpyInteger, SPYINTEGER, SpyInteger, SpyString, SPYSTRING, SpyInteger);


// logical operator
#undef DEF_OPER




class OperPower : public BinaryOper 
{ 
	protected: 
		AutoPtr<SpyObj>calc( 
				AutoPtr<SpyObj>op0, 
				AutoPtr<SpyObj>op1) 
		{ 
			if (op0.isTypeOf(SpyInteger()) && op1.isTypeOf(SpyInteger())) 
			{ 
				AutoPtr<SpyInteger>top0, top1;
				top0.copyFrom(op0);
				top1.copyFrom(op1);

				SpyInteger a = *top0,
						   b = *top1,
						   ans = 1;

				while (b != 0)
				{
					if (b % 2 != 0)
						ans = ans * a;
					a = a * a;
					b = b / 2;
				}


				return new SpyInteger(ans); 
			} else throw SpyRuntimeExc("operator `**' can not be applied"); 
			return new SpyVoid(); 
		} 
	public: 
		OperPower (AutoPtr<AstNode>oprnd0, AutoPtr<AstNode>oprnd1) : 
			BinaryOper(oprnd0, oprnd1) {} 
};


}

#endif // __INCLUDE_AST_BINARY_OPER__
