/*
 * $File: unary_oper.cxx
 * $Date: Wed May 23 21:47:09 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "unary_oper.hxx"

using namespace ast;

OperUnaryMinus::OperUnaryMinus(AutoPtr<AstNode>oprnd_)
{
	oprnd.copyFrom(oprnd_);
	if (oprnd == NULL)
		throw SpyRuntimeExc("operands of unary minus must be expression");
}

AutoPtr<SpyObj> OperUnaryMinus::execute(AutoPtr<VariablePool> vpool)
{
	AutoPtr<SpyInteger> op;
	op.copyFrom(oprnd->execute(vpool));

	if (op == NULL)
		throw SpyRuntimeExc("unary minus must be applied on integers");;
	return new SpyInteger((*op) * (-1));
}

