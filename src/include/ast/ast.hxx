/*
 * $File: ast.hxx
 * $Date: Fri May 25 23:44:18 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */


#ifndef __INCLUDE_AST__
#define __INCLUDE_AST__

#include <vpool.hxx>
#include <spyobj.hxx>
#include <exception>
#include <vector>
#include <auto_ptr.hxx>
#include <map>

#include "typeid.hxx"

namespace ast
{


	/**
	 * the default `AstNode` act as a 'statement'
	 */
	class AstNode
	{
		public:
			AutoPtr<SpyObj> value;
			std::vector<AutoPtr<AstNode>> child;
			AstNode();
			AstNode(const std::vector<AutoPtr<AstNode>> &child);

			virtual AutoPtr<SpyObj> execute(AutoPtr<VariablePool> vpool);

			virtual ~AstNode() {}
	};

	class Ast
	{
		protected:

			AutoPtr<AstNode> root;

		public:
			void execute();
			void setRoot(AutoPtr<AstNode>root);
			virtual ~Ast();
	};
}

#endif // __INCLUDE_AST__
