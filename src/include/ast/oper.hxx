/*
 * $File: oper.hxx
 * $Date: Mon May 21 21:45:00 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_OPER_WRAPPER__
#define __INCLUDE_AST_OPER_WRAPPER__

#include "oper/oper.hxx"
#include "oper/unary_oper.hxx"
#include "oper/binary_oper.hxx"

#endif // __INCLUDE_AST_OPER_WRAPPER__
