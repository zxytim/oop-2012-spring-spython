/*
 * $File: cond.hxx
 * $Date: Mon May 21 19:15:08 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_COND_WRAPPER__
#define __INCLUDE_AST_COND_WRAPPER__

#include "cond/if.hxx"
#include "cond/while.hxx"

#endif // __INCLUDE_AST_COND_WRAPPER__

