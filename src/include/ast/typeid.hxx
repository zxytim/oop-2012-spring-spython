/*
 * $File: typeid.hxx
 * $Date: Wed May 23 22:03:08 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_TYPEID__
#define __INCLUDE_AST_TYPEID__

enum AstNodeTypeID
{
	ASTNODE_EXPR
};

#endif // __INCLUDE_AST_TYPEID__
