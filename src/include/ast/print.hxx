/*
 * $File: print.hxx
 * $Date: Sun May 20 21:46:48 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_PRINT__
#define __INCLUDE_AST_PRINT__

#include "ast.hxx"

namespace ast
{
	class Print : public AstNode
	{
		public:
			bool new_line;
			Print(){}
			Print(const std::vector<AutoPtr<AstNode>> &child, 
					bool new_line = false) :
				AstNode(child), new_line(new_line) {}
			virtual AutoPtr<SpyObj>execute(AutoPtr<VariablePool> vpool);
	};
}

#endif // __INCLUDE_AST_PRINT__

