/*
 * $File: variable.hxx
 * $Date: Sun May 20 21:57:19 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_AST_VARIABLE__
#define __INCLUDE_AST_VARIABLE__

#include "ast.hxx"

#include <string>

namespace ast
{
	class Variable : public AstNode
	{
		public:
			std::string name;
			Variable(AutoPtr<SpyObj>obj);
			virtual AutoPtr<SpyObj>execute(AutoPtr<VariablePool> vpool);
	};
}

#endif // __INCLUDE_AST_VARIABLE__
