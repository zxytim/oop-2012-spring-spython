/*
 * $File: assign.cxx
 * $Date: Wed May 23 21:57:28 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "assign.hxx"
#include <sassert.hxx>

using namespace ast;

AutoPtr<SpyObj>Assign::execute(AutoPtr<VariablePool> vpool)
{
	sassert(child.size() == 1);
#ifdef __DEBUG_VERBOSE
	fprintf(stderr, "walk:assign\n");
#endif 

	AutoPtr<SpyObj>val = child[0]->execute(vpool);
	vpool->assignVarValue(var->name, val);
	return val;
}

Assign::Assign(AutoPtr<AstNode>var_, AutoPtr<AstNode>val_)
{
	var.copyFrom(var_);
	AutoPtr<AstNode> val; // expr XXX
	val.copyFrom(val_);

	child.push_back(val);
}

