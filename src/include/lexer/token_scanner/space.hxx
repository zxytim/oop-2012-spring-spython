/*
 * $File: space.hxx
 * $Date: Sat May 19 18:53:36 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_LEXER_TOKEN_SCANNER_SPACE__
#define __INCLUDE_LEXER_TOKEN_SCANNER_SPACE__

#include "token_scanner.hxx"

// **Space** here Means: seperator of lines
//			with optional Leading Space
class TokenScannerSpace : public TokenScanner
{
	public:
		void scan(const char *src);
};

#endif // __INCLUDE_LEXER_TOKEN_SCANNER_SPACE__
