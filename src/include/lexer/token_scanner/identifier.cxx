/*
 * $File: identifier.cxx
 * $Date: Fri May 18 16:05:42 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "identifier.hxx"
#include <cctype>

void TokenScannerIdentifier::scan(const char *src)
{
	TokenIdentifier id;

	char ch = *src;
	if (!(isalpha(ch) || ch == '_'))
		return;

	id.name.append(1, ch);
	src ++;

	for (; ;)
	{
		ch = *src;
		if (!(isalnum(ch) || ch == '_'))
			break;
		src ++;
		id.name.append(1, ch);
	}

	ScanData data;
	data.token = TOKEN_IDENTIFIER;
	data.src = src;
	data.data = new TokenIdentifier(id);

	throw data;

}
