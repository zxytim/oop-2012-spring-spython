/*
 * $File: token_scanner.hxx
 * $Date: Sat May 19 19:59:15 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_LEXER_TOKEN_SCANNER__
#define __INCLUDE_LEXER_TOKEN_SCANNER__

#include <algorithm>
#include <token.hxx>
#include <auto_ptr.hxx>

struct ScanData
{
	int token;
	const char *src;
	AutoPtr<Token> data;

	int line; // number of lines proceeded

	ScanData() : 
		token(-1), src(NULL), data(NULL), line(0) {}
	//AutoPtr<void *> data;
};

class TokenScanner
{
	public:

		/**
		 * ScanData
		 *		token -> token
		 *		src -> the pointer right after
		 *			the last of token
		 *		data -> repective data
		 */
		virtual void scan(const char *src) = 0;
		virtual ~TokenScanner() {}
};

#endif // __INCLUDE_LEXER_TOKEN_SCANNER__
