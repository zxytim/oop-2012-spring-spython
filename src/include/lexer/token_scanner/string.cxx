/*
 * $File: string.cxx
 * $Date: Sat May 19 21:03:17 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "string.hxx"
#include <token.hxx>
#include <exception.hxx>
#include <spyobj/spystring.hxx>

void TokenScannerString::scan(const char *src)
{
	if (*src != '\"' && *src != '\'')
		return;

	char term = *src;
	std::string str;

	ScanData data;
	data.line = 0;

	src ++;
	for (;;)
	{
		char ch = *(src ++);
		if (ch == '\n' || ch == '\0')
			SPY_SYNTAX_ERROR("EOL while scanning string literal");
		if (ch == '\\')
		{
			char nch = *(src ++);
			if (nch == '\\')
				str.append(1, '\\');
			else if (nch == 'n')
				str.append(1, '\n');
			else if (nch == '\n')
			{
				data.line ++;
				continue;
			}
			else if (nch == '\0')
				str.append(1, '\\');
			else if (nch == '\"')
				str.append(1, '\"');
			else if (nch == '\'')
				str.append(1, '\'');
			else
			{
				str.append(1, '\\');
				str.append(1, nch);
			}
			continue;
		}

		if (ch == term)
			break;
		// ch != '\\'
		str.append(1, ch);
	}

	TokenString tstr;
	tstr.str = str;
	data.token = TOKEN_STRING;
	data.src = src;
	data.data = new TokenString(tstr);

	throw data;
}

