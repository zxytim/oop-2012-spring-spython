/*
 * $File: identifier.hxx
 * $Date: Fri May 18 10:47:23 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_LEXER_TOKEN_SCANNER_IDENTIFIER__
#define __INCLUDE_LEXER_TOKEN_SCANNER_IDENTIFIER__


#include "token_scanner.hxx"

class TokenScannerIdentifier: public TokenScanner
{
	public:
		void scan(const char *src);
};


#endif // __INCLUDE_LEXER_TOKEN_SCANNER_IDENTIFIER__
