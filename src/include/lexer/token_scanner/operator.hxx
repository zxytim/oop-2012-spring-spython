/*
 * $File: operator.hxx
 * $Date: Fri May 18 10:54:47 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_LEXER_TOKEN_SCANNER_OPERATOR__
#define __INCLUDE_LEXER_TOKEN_SCANNER_OPERATOR__

#include "token_scanner.hxx"

class TokenScannerOperator: public TokenScanner
{
	public:
		void scan(const char *src);
};

#endif // __INCLUDE_LEXER_TOKEN_SCANNER_OPERATOR__
