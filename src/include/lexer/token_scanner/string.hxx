/*
 * $File: string.hxx
 * $Date: Fri May 18 10:06:50 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_LEXER_TOKEN_SCANNER_STRING__
#define __INCLUDE_LEXER_TOKEN_SCANNER_STRING__

#include "token_scanner.hxx"

class TokenScannerString : public TokenScanner
{
	public:
		void scan(const char *src);
};

#endif // __INCLUDE_LEXER_TOKEN_SCANNER_STRING__
