/*
 * $File: comment.hxx
 * $Date: Fri May 18 12:20:58 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_LEXER_TOKEN_SCANNER_COMMENT__
#define __INCLUDE_LEXER_TOKEN_SCANNER_COMMENT__


#include "token_scanner.hxx"

class TokenScannerComment: public TokenScanner
{
	public:
		void scan(const char *src);
};

#endif // __INCLUDE_LEXER_TOKEN_SCANNER_COMMENT__
