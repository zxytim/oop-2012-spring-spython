/*
 * $File: newline.hxx
 * $Date: Fri May 18 10:52:03 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_LEXER_TOKEN_SCANNER_NEWLINE__
#define __INCLUDE_LEXER_TOKEN_SCANNER_NEWLINE__

#include "token_scanner.hxx"

class TokenScannerNewLine: public TokenScanner
{
	public:
		void scan(const char *src);
};

#endif // __INCLUDE_LEXER_TOKEN_SCANNER_NEWLINE__
