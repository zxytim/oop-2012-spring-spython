/*
 * $File: integer.hxx
 * $Date: Thu May 17 23:29:24 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_LEXER_TOKEN_SCANNER_INTEGER__
#define __INCLUDE_LEXER_TOKEN_SCANNER_INTEGER__

#include "token_scanner.hxx"

class TokenScannerInteger : public TokenScanner
{
	public:
		void scan(const char *src);
};

#endif // __INCLUDE_LEXER_TOKEN_SCANNER_INTEGER__ 
