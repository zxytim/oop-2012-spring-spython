/*
 * $File: integer.cxx
 * $Date: Fri May 18 16:10:23 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "integer.hxx"
#include <cctype>
#include <exception.hxx>

void TokenScannerInteger::scan(const char *src)
{
	std::string num;
#if 0
	// **SIGN** should not be considered here
	if (*src == '+' || *src == '-')
	{
		if (*src == '-')
			num.append(1, '-');
	}

	while (*src == ' ' || *src == '\t')
		src ++;

#endif
	// TODO: integer only
	//	float can be '.'
	if (!isdigit(*src))
		return;
	
	while (isdigit(*src))
		num.append(1, *(src ++));

	TokenNumber tnum;
	tnum.number = num;

	ScanData data;
	data.token = TOKEN_NUMBER;
	data.src = src;
	data.data = new TokenNumber(tnum);

	throw data;
}

