/*
 * $File: space.cxx
 * $Date: Sat May 19 21:03:30 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "space.hxx"
#include <token.hxx>

void TokenScannerSpace::scan(const char *src)
{
	ScanData data;
	TokenSpace ss;

	if (*src != ' ' && *src != '\t')
		return;

	ss.type = TOKENSPACE_NONE;
	ss.cnt = 0;
	do
	{
		char base = *src;
		if (base == ' ')
			ss.type = TOKENSPACE_SPACE;
		else if (base == '\t')
			ss.type = TOKENSPACE_TAB;
		else break;

		while (*src == base)
			ss.cnt ++, src ++;

	} while(0);

	data.token = TOKEN_SPACE;
	data.src = src;
	data.data = new TokenSpace(ss);

	throw data;
}
