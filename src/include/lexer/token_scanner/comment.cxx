/*
 * $File: comment.cxx
 * $Date: Fri May 18 16:04:52 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "comment.hxx"

void TokenScannerComment::scan(const char *src)
{
	TokenComment tc;
	char ch = *src;
	if (ch != '#')
		return;
	for (; ;)
	{
		ch = *src;
		if (ch == '\n')
			break;
		src ++;
		tc.content.append(1, ch);
	}

	ScanData data;
	data.token = TOKEN_COMMENT;
	data.src = src;
	data.data = new TokenComment(tc);

	throw data;
}

