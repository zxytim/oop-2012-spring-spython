/*
 * $File: operator.cxx
 * $Date: Fri May 18 22:22:15 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "operator.hxx"
#include <string>

void TokenScannerOperator::scan(const char *src)
{
	for (size_t len = 3; len >= 1; len --)
	{
		for (auto &op : OPERATOR)
		{
			if (op.first.length() != len)
				continue;
			std::string str;
			for (size_t i = 0; i < op.first.length(); i ++)
				str.append(1, *(src + i));
			if (str == op.first)
			{
				TokenOperator oper;
				oper.opid = op.second;

				ScanData data;
				data.token = TOKEN_OPERATOR;
				data.src = src + op.first.length();
				data.data = new TokenOperator(oper);

				throw data;
			}
		}
	}
}

