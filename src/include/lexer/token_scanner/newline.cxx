/*
 * $File: newline.cxx
 * $Date: Wed May 30 23:36:46 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "newline.hxx"

void TokenScannerNewLine::scan(const char *src)
{
	if (!(*src == '\n' || *src == '\r'))
		return;
	//src ++;
	// new version start from here
	// {
	if (*src == '\n')
	{
		src ++;
		if (*(src + 1) == '\r')
			src ++;
	}
	else  // \r
	{
		src ++;
		if (*(src + 1) == '\n')
			src ++;
	}
	// }

	TokenNewLine nl;

	do
	{
		char base = *src;
		if (base == ' ')
			nl.stype = TOKENSPACE_SPACE;
		else if (base == '\t')
			nl.stype = TOKENSPACE_TAB;
		else break;

		while (*src == base)
			nl.scnt ++, src ++;

	} while(0);

	ScanData data;

	data.token = TOKEN_NEWLINE;
	data.src = src;
	data.line = 1;
	data.data = new TokenNewLine(nl);

	throw data;
}
