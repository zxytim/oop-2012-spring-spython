/*
 * $File: lexer.hxx
 * $Date: Wed May 23 23:09:30 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_LEXER__
#define __INCLUDE_LEXER__

#include <cstdio>
#include <vector>


#include <auto_ptr.hxx>
#include "token_scanner.hxx"

class Lexer
{
	protected:
		std::vector<AutoPtr<TokenScanner> > scanner;
		char *src, *src_start, *src_end;
		const char *srcptr;

		int line;

		void registerTokenScanner(AutoPtr<TokenScanner> ptr);
		//void registerTokenScanner(TokenScanner *ptr);
	public:
		Lexer();
		~Lexer();

		void initFile(FILE *file);

		void next_token();

		bool finished();
		void error();
		void error(const char *msg);
};

#endif // __INCLUDE_LEXER__
