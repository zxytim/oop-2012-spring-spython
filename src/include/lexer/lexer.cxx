/*
 * $File: lexer.cxx
 * $Date: Wed May 30 23:35:52 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "lexer.hxx"
#include "token_scanner.hxx"
#include <exception.hxx>

#include <cstdlib>

namespace LexerImpl
{
	void error(const char *msg)
	{
		fprintf(stderr, "%s\n", msg);
		exit(1);
	}
}

using namespace LexerImpl;

//void Lexer::registerTokenScanner(AutoPtr<TokenScanner> ptr)
void Lexer::registerTokenScanner(AutoPtr<TokenScanner> ptr)
{
	scanner.push_back(ptr);
}

Lexer::Lexer()
{
	// -1
	registerTokenScanner(new TokenScannerComment());
	
	// 0
	registerTokenScanner(new TokenScannerInteger());
	registerTokenScanner(new TokenScannerString());

	registerTokenScanner(new TokenScannerNewLine());
	
	// 1
	registerTokenScanner(new TokenScannerSpace());


	// 2
	registerTokenScanner(new TokenScannerIdentifier());

	// 3
	registerTokenScanner(new TokenScannerOperator());

	line = 1;
}


void Lexer::initFile(FILE *file)
{
	if (file == NULL)
		LexerImpl::error("null file pointer");


	fseek(file,	0, SEEK_END);
	size_t fsize = ftell(file);
	fseek(file, 0, SEEK_SET);

	int extra = 100;
	src = new char [fsize + extra];
	memset(src, 0, sizeof(char) * (fsize + extra));

	src_start = src + extra / 2;
	fread(src_start, fsize, 1, file);
	src_end = src_start + fsize;
	while (*src_end == 0 && src_end >= src_start)
		src_end --;
	src_end ++;

	srcptr = src_start;
}

bool Lexer::finished()
{
	return srcptr >= src_end;
}


void Lexer::error()
{
	fprintf(stderr, "Syntax Error, around line %d\n", line);
	exit(1);
}

void Lexer::error(const char *msg)
{
	fprintf(stderr, "Syntax Error, around line %d: %s\n", line, msg);
	exit(1);
}

void Lexer::next_token()
{
	try
	{
		for (auto &p: scanner)
			p->scan(srcptr);
	}
	catch (ScanData &data)
	{
		line += data.line;
		srcptr = data.src;
		data.line = line;
		throw data;
	}
	catch (SpyExc &e)
	{
		fprintf(stderr, "line around %d: %s\n", line, e.msg());
	}
}


Lexer::~Lexer()
{
	delete [] src;
}

