/*
 * $File: token_scanner.hxx
 * $Date: Sat May 19 20:41:52 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_LEXER_TOKEN_SCANNER_WRAPPER__
#define __INCLUDE_LEXER_TOKEN_SCANNER_WRAPPER__

#include "token_scanner/token_scanner.hxx"
#include "token_scanner/integer.hxx"
#include "token_scanner/space.hxx"
#include "token_scanner/newline.hxx"
#include "token_scanner/operator.hxx"
#include "token_scanner/identifier.hxx"
#include "token_scanner/string.hxx"
#include "token_scanner/comment.hxx"

#endif // __INCLUDE_LEXER_TOKEN_SCANNER_WRAPPER__
