/*
 * $File: ast.hxx
 * $Date: Mon May 21 17:19:55 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */


#ifndef __INCLUDE_AST_WRAPPER__
#define __INCLUDE_AST_WRAPPER__


#include "ast/ast.hxx"
#include "ast/assign.hxx"
#include "ast/expr.hxx"
#include "ast/print.hxx"
#include "ast/variable.hxx"
#include "ast/oper.hxx"
#include "ast/cond.hxx"


#endif // __INCLUDE_AST_WRAPPER__
