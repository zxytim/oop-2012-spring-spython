/*
 * $File: spyobj.hxx
 * $Date: Sun May 20 15:39:35 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_SPYOBJ_WRAPPER__
#define __INCLUDE_SPYOBJ_WRAPPER__

#include "spyobj/spyinteger.hxx"
#include "spyobj/spystring.hxx"
#include "spyobj/spyvariable.hxx"
#include "spyobj/spyvoid.hxx"

#endif // __INCLUDE_SPYOBJ_WRAPPER__
