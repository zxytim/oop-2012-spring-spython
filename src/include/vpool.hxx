/*
 * $File: vpool.hxx
 * $Date: Wed May 30 20:24:44 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __INCLUDE_VPOOL__
#define __INCLUDE_VPOOL__


#include <string>
#include <auto_ptr.hxx>
#include <map>
#include <cstdlib>

class SpyObj;

class VariablePool
{
	public:
		struct Data
		{
			AutoPtr<char> data;
			size_t size;
			Data() : data(NULL) {}
		};
		std::map<std::string, Data> var_mem;

		void assignVarValue(const std::string &name, AutoPtr<SpyObj>value);
		AutoPtr<SpyObj>fetchVarValue(const std::string &name);

		~VariablePool();
};

#endif // __INCLUDE_VPOOL__
