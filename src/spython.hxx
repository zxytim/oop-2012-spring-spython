/*
 * $File: spython.hxx
 * $Date: Wed May 23 22:46:46 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __SPYTHON_BASE__
#define __SPYTHON_BASE__

#include <common.hxx>
#include <vector>
#include <string>
#include <syntax/yacc.hxx>
#include <ast.hxx>

class SpythonInterpreter
{
	protected:
		/**
		 * for yacc use
		 */
		int yylex();

		virtual void lexicalAnalysis(FILE *file);
		virtual void syntaxAnalysis();
		virtual void sematicAnalysis();
		virtual void cleanUp();

		std::vector<AutoPtr<Token>> tokens;

		AutoPtr<Token> cur_token;


		/**
		 * Abstract Syntax Tree
		 */
		AutoPtr<ast::Ast> ast_tree;
		void doWalk(AutoPtr<ast::AstNode>root);


	public:
		// TODO: singleton
		
		friend int YACCImpl::yylex();
		void interpret(FILE *file);

		SpythonInterpreter();
		~SpythonInterpreter();
};

#endif // __SPYTHON_BASE__
