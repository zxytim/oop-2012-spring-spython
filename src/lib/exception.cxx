/*
 * $File: exception.cxx
 * $Date: Sat May 19 21:16:51 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "exception.hxx"
#include <cstdarg>


const char *SpyExcImpl::excprintf(const char *fmt, ...)
{
	static const int MSG_LEN_MAX = 100000;
	static char msg_buf[MSG_LEN_MAX + 1];
	va_list args;
	va_start(args, fmt);
	vsnprintf(msg_buf, MSG_LEN_MAX, fmt, args);
	va_end(args);

	return msg_buf;
}

SpyExc::SpyExc()
{
	m_msg = "no exception message given";
}

SpyExc::SpyExc(const std::string &msg) :
	m_msg(msg)
{
}

const char *SpyExc::msg() const
{
	return m_msg.c_str();
}

