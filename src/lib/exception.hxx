/*
 * $File: exception.hxx
 * $Date: Sun May 20 14:23:06 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __LIB_EXCEPTION__
#define __LIB_EXCEPTION__ 
#include <string>
#include <cstdarg>

class SpyExc
{
	protected:
		std::string m_msg;
	public:
		SpyExc();
		SpyExc(const std::string &msg);
		const char *msg() const;
};

#define DEF_ERR_EXC(name, inherit, prompt) \
	class name : public inherit \
	{ \
		public: \
			name(const char *fmt, ...) \
			{ \
				static const int MSG_LEN_MAX = 100000; \
				static char msg_buf[MSG_LEN_MAX + 1]; \
				va_list args; \
				va_start(args, fmt); \
				vsnprintf(msg_buf, MSG_LEN_MAX, fmt, args); \
				va_end(args); \
				SpyExc::m_msg = (std::string)(prompt) + msg_buf; \
			} \
			name(const std::string &msg) : inherit(prompt + msg) {} \
	} 

DEF_ERR_EXC(SpyExcSyntaxError, SpyExc, "Syntax Error");
DEF_ERR_EXC(SpyRuntimeExc, SpyExc, "Runtime Error");

namespace SpyExcImpl
{
	const char *excprintf(const char *fmt, ...);
}

#define SPY_SYNTAX_ERROR(...) \
	do { \
		throw SpyExcSyntaxError(SpyExcImpl::excprintf(__VA_ARGS__)); \
	} while (0)

#endif // __LIB_EXCEPTION__
