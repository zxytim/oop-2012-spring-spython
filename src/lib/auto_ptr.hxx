/*
 * $File: auto_ptr.hxx
 * $Date: Wed May 30 20:42:46 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __LIB_AUTO_PTR__
#define __LIB_AUTO_PTR__

#include "sassert.hxx"
#include <cstring>

class AutoPtrBase
{
	public: 
		// the actual pointer
		char *m_ptr;

		// number of used reference,
		// the lowest bit is used as delete flag
		size_t * m_use; 

};

template<typename T>
class AutoPtr : public AutoPtrBase
{
	private:
		void remove_ref()
		{
			if (((*m_use) -= 2) < 2)
			{
				T *real_ptr = reinterpret_cast<T *>(m_ptr);
				if (*m_use)
					delete [] real_ptr;
				else delete real_ptr;
				delete m_use;
#ifdef __DEBUG_BUILD
				m_ptr = NULL;
				m_use = NULL;
#endif
			}
		}

	public:
		AutoPtr(T *ptr = nullptr)
		{
			 m_ptr = reinterpret_cast<char *>(ptr);
			 m_use = new size_t(2);
		}

		AutoPtr(T *ptr, bool is_array)
		{
			 m_ptr = reinterpret_cast<char *>(ptr);
			 m_use = new size_t(2 + is_array);
		}

		~AutoPtr()
		{
			remove_ref();
		}

		AutoPtr(const AutoPtr<T> &ptr)
		{
			m_ptr = reinterpret_cast<char *>(ptr.m_ptr);
			m_use = ptr.m_use;
			(*m_use) += 2;
		}

		AutoPtr<T> & operator = (const AutoPtr<T> &ptr)
		{
			(*ptr.m_use) += 2; // protect against self-assignment
			remove_ref(); // decrement use count and delete pointers if needed

			m_ptr = ptr.m_ptr;
			m_use = ptr.m_use;

			return *this;
		}

		inline T& operator * ()
		{
			sassert(m_ptr);
			return *reinterpret_cast<T *>(m_ptr);
		}

		inline const T& operator *() const
		{
			sassert(m_ptr);
			return *reinterpret_cast<T *>(m_ptr);
		}


		inline T* operator -> ()
		{
			sassert(m_ptr);
			return reinterpret_cast<T *>(m_ptr);
		}


		inline const T* operator -> () const
		{
			sassert(m_ptr);
			return reinterpret_cast<T *>(m_ptr);
		}

		inline bool operator == (const AutoPtr<T> &ptr) const
		{
			return m_ptr == ptr.m_ptr;
		}

		inline bool operator != (const AutoPtr<T> &ptr) const
		{
			return m_ptr != ptr.m_ptr;
		}

		inline T * ptr()
		{
			return reinterpret_cast<T *>(m_ptr);
		}

		inline T * ptr() const
		{
			return reinterpret_cast<T *>(m_ptr);
		}

		// force convert
		inline void copyFrom(AutoPtrBase ptr)
		{
			(*ptr.m_use) += 2; // protect against self-assignment
			remove_ref(); // decrement use count and delete pointers if needed

			m_ptr = ptr.m_ptr;
			m_use = ptr.m_use;
		}

		template<typename Other>
		bool isTypeOf(const Other &other)
		{
			const Other *a = &other;
			const Other *b = a;
			a = b;
			Other *optr = dynamic_cast<Other *>(ptr());
			return optr != NULL;
		}

};

#endif // __LIB_AUTO_PTR__

/**
 * vim: ft=cpp
 */
