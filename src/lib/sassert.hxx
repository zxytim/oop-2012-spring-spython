/*
 * $File: sassert.hxx
 * $Date: Thu May 17 18:32:10 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */


#ifndef __LIB_SASSERT__
#define __LIB_SASSERT__

#include <cassert>

#ifdef __DEBUG_BUILD
#define sassert(expr) assert(expr)
#else
#define sassert(expr)
#endif

#endif // __LIB_SASSERT__
