/*
 * $File: misc.cxx
 * $Date: Fri May 18 15:46:37 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "common.hxx"

#include <cstdarg>

std::string strprintf(const char *fmt, ...)
{
	static const int MSG_LEN_MAX = 100000;
	static char msg_buf[MSG_LEN_MAX + 1];
	va_list args;
	va_start(args, fmt);
	vsnprintf(msg_buf, MSG_LEN_MAX, fmt, args);
	va_end(args);

	return msg_buf;
}

