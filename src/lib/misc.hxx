/*
 * $File: misc.hxx
 * $Date: Fri May 18 15:46:34 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __LIB_MISC__
#define __LIB_MISC__

#include <string>

std::string strprintf(const char *fmt, ...);

#endif // __LIB_MISC__
