/*
 * $File: spython.cxx
 * $Date: Wed May 23 23:50:47 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "spython.hxx"
#include <exception.hxx>

#include <stack>

using namespace std;

SpythonInterpreter::SpythonInterpreter()
{
	ast_tree = new ast::Ast();
}

void SpythonInterpreter::interpret(FILE *file)
{
	/**
	 * Procedure:
	 *		1. lexical analysis
	 *		2. syntax analysis --> AST
	 *		3. interpret(AST): sematic analysis
	 */

	try
	{
		lexicalAnalysis(file);
		syntaxAnalysis();
		sematicAnalysis();

		cleanUp();
	}
	catch (SpyExc &e)
	{
		fprintf(stderr, "Error: %s\n", e.msg());
	}
}

int SpythonInterpreter::yylex()
{
	static bool first = true;
	static size_t cur_id = 0;
	static size_t ntoken = tokens.size();

	if (first)
	{
		cur_id = 0;
		first = false;
	}

	if (cur_id == ntoken)
		return 0;
	cur_token = tokens[cur_id];

	int ret = cur_token->getTokenID();
	cur_id ++;
	return ret;
}


void SpythonInterpreter::lexicalAnalysis(FILE *file)
{
	// 1.
	Lexer lexer;
	lexer.initFile(file);

	bool sfirst = true;
	int stype = TOKENSPACE_NONE;

	stack<int> sstack;

	sstack.push(0); // initial hierarchy 

	tokens.clear();
	tokens.push_back(new TokenBracket(TOKEN_LBRACKET));

	for (int cnt; ; cnt ++)
	{
		if (lexer.finished())
			break;
		try { lexer.next_token(); }
		catch (ScanData & data)
		{
			AutoPtr<Token> token = data.data;

			if (token->type == TOKEN_COMMENT)
				continue;
			// space is useless
			if (token->type == TOKEN_SPACE)
				continue;

			if (token->type != TOKEN_NEWLINE)
			{
				tokens.push_back(token);
#ifdef __DEBUG_VERBOSE
				fprintf(stderr, "[%3d]",
					   	(int)tokens.size() - 1);
				tokens.back()->printInfo();
#endif
				continue;
			}
			TokenNewLine *tnl = dynamic_cast<TokenNewLine *>(token.ptr());

			if (sfirst)
			{
				if (tnl->stype != TOKENSPACE_NONE)
				{
					stype = tnl->stype;
					sfirst = false;
				}
			}

			if (stype == TOKENSPACE_NONE)
				assert(tnl->stype == TOKENSPACE_NONE);

			if (stype != TOKENSPACE_NONE)
			{
				if (tnl->stype != TOKENSPACE_NONE)
					if (tnl->stype != stype)
						lexer.error("multiple type of spaces");
			}

			do
			{
				tokens.push_back(token);
				// > ascending hierarchy
				if (tnl->scnt > sstack.top())
				{
					sstack.push(tnl->scnt);
					tokens.push_back(new TokenBracket(TOKEN_LBRACKET));
					break;
				}
				// < descending hierarchy
				while(!sstack.empty())
				{
					if (sstack.top() == tnl->scnt)
						break;
					tokens.push_back(new TokenBracket(TOKEN_RBRACKET));
					sstack.pop();
				}
				if (sstack.empty())
					lexer.error("Unexpected space indent");
			} while (0);
			//fprintf(stderr, "src: %s\n", data.src);

#ifdef __DEBUG_VERBOSE
			fprintf(stderr, "[%3d]", (int)tokens.size() - 1);
			tokens.back()->printInfo();
#endif

			continue;
		}
		//fprintf(stderr, "<%3d>\n", cnt);
		lexer.error();
	}

	tokens.push_back(new TokenBracket(TOKEN_RBRACKET));
#ifdef __DEBUG_VERBOSE
	int ntok = 0;
	for (auto &t: tokens)
	{
		fprintf(stderr, "tokens[%3d] ", ntok ++);
		t->printInfo();
	}
	fprintf(stderr, "lexical analysis succeed.\n");
#endif
}

void SpythonInterpreter::sematicAnalysis()
{
	ast_tree->execute();
}


void SpythonInterpreter::cleanUp()
{
}

namespace SpythonInterpreterImpl
{
}

using namespace SpythonInterpreterImpl;

void SpythonInterpreter::syntaxAnalysis()
{
	yyinit(this);

	yaccparse(ast_tree);
}

SpythonInterpreter::~SpythonInterpreter()
{
}

