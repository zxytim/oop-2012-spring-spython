#
# $File: Makefile
# $Date: Wed May 30 21:15:51 2012 +0800
#
# A single output portable Makefile for
# simple c++ project

OBJ_DIR = obj
BIN_DIR = bin
TARGET = spython

BIN_TARGET = $(BIN_DIR)/$(TARGET)

PKGCONFIG_LIBS = 
INCLUDE_DIR = -I src/include -I src -I src/lib
DEFINES = -D__DEBUG_BUILD 
DEFINES += #-D__DEBUG_VERBOSE


CXXFLAGS += #-g -O0 #-pg
CXXFLAGS += -O3
CXXFLAGS += $(DEFINES)
CXXFLAGS += -std=c++11
CXXFLAGS += -Wall -Wextra 
CXXFLAGS += $(INCLUDE_DIR) \

LDFLAGS = 

CXX = g++
YACC = yacc

YACCFLAGS = -v --warning=all

YACCSOURCE = $(shell find src -name "*.y")
YACCCXX = $(YACCSOURCE:.y=.cxx)
YACCOUTPUT = $(YACCCXX) $(YACCSOURCE:.y=.hxx)

TOKEN_DEF_HXX = src/include/token_def.hxx

CXXSOURCES = $(shell find src -name "*.cxx" | sed -e "/syntax\.cxx/d")

CXXSOURCES += $(YACCCXX)

OBJS = $(addprefix $(OBJ_DIR)/,$(CXXSOURCES:.cxx=.o))
DEPFILES = $(OBJS:.o=.d)

.PHONY: all clean run rebuild gdb

all: $(BIN_TARGET)

$(TOKEN_DEF_HXX) $(YACCCXX): $(YACCSOURCE)
	echo "[yacc] $< ..." && \
	mkdir -pv $(dir /tmp/$(TOKEN_DEF_HXX)) && \
	$(YACC)  $(YACCFLAGS) --output=$@ $< --defines=/tmp/$(TOKEN_DEF_HXX) && \
	head -n`grep "#define" /tmp/$(TOKEN_DEF_HXX) -n | tail -n 1 | sed -e 's/^\([0-9]*\).*$$/\1/g'` /tmp/$(TOKEN_DEF_HXX) > $(TOKEN_DEF_HXX)  


$(OBJ_DIR)/%.o: %.cxx
	@echo "[cxx] $< ..."
	@$(CXX) -c $< $(CXXFLAGS) -o $@

$(OBJ_DIR)/%.d: %.cxx
	@mkdir -pv $(dir $@)
	@echo "[dep] $< ..."
	@$(CXX) $(INCLUDE_DIR) $(CXXFLAGS) -MM -MT "$(OBJ_DIR)/$(<:.cxx=.o) $(OBJ_DIR)/$(<:.cxx=.d)" "$<" > "$@"

sinclude $(DEPFILES)

$(BIN_TARGET): $(OBJS)
	@echo "[link] $< ..."
	@mkdir -p $(BIN_DIR)
	@$(CXX) $(OBJS) -o $@ $(LDFLAGS) $(CXXFLAGS)

clean:
	rm -rf $(OBJ_DIR) $(BIN_DIR) $(YACCOUTPUT)
	+cd doc && make clean

run: $(BIN_TARGET)
	./$(BIN_TARGET)

rebuild:
	+@make clean 
	+@make

gdb: $(BIN_TARGET)
	gdb ./$(BIN_TARGET)

commit:
	make clean
	git add .
	git add . -u
	git commit

push:
	make commit
	git push
