s0 = "Hello"
s1 = "World"
print s0 + " " + s1

print "This is an mu\
lti-line string\
but you can not reali\
ze it from th\
e output, \nhere is a back slash \\\\ -> `\\'\
, a tab \\t -> `\t', a carrige return \\n -> `\n', \
a double quote \\\" -> `\"\
'\
they looks cool right? \n\
Bazinga! Hahaha!\
\n\
"

if 1 * 2 + 3 * (4 + 5) < 1 * 2 + 3 * (4 + 5) + 1:
    print "Compare operator test succeed"

print 1 + 2 * 30 / 4 * 2 ** (10 - 3) % 930125

# Product Table
print "Product Table"
i = 1
while i <= 5:
    j = 1
    while j <= i:
        print i, "*", j, "=", i * j, "   ",
        j = j + 1
    print 
    i = i + 1


# Prime Generation
n = 1000
print "Prime generation: primes up to", n
i = 2
nprime = 0
while i <= n:
    j = 2
    is_prime = True
    while j * j <= i:
        if i % j == 0:
            is_prime = False
            break
        j = j + 1
    if is_prime:
        print i,
        nprime = nprime + 1
        if (nprime % 10 == 0):
            print
    i = i + 1

print

